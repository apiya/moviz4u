<?php

namespace App\EventListener;

use App\Entity\User;
use Symfony\Component\HttpKernel\HttpKernel;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


class ActivityListener
{

    protected $securityContext;
    protected $entityManager;

    public function __construct(TokenStorageInterface $securityContext, ObjectManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->securityContext = $securityContext;
    }



    /**
     * Update the user "lastActivity" on each request
     * @param FilterControllerEvent $event
     */


    public function onCoreController(FilterControllerEvent $event)
    {

        // Check that the current request is a "MASTER_REQUEST"
        // Ignore any sub-request
        if ($event->getRequestType() !== HttpKernel::MASTER_REQUEST) {
            return;
        }

        // Check token authentication availability
        if ($this->securityContext->getToken()) {
            $user = $this->securityContext->getToken()->getUser();

            if (($user instanceof User) && !($user->isActiveNow())) {
                $user->setIsOnline(new \DateTime('now'));
                $this->entityManager->flush($user);
            }

        }

    }

}