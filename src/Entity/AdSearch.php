<?php

namespace App\Entity;


class AdSearch
{

    /**
     * @var String
     */
    private $title;

    /**
     * @var int
     */
    private $rooms;


    /**
     * Get the value of title
     *
     * @return  String
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @param  String  $title
     *
     * @return  self
     */ 
    public function setTitle(String $title)
    {
        $this->title = $title;

        return $this;
    }



    /**
     * Get the value of rooms
     *
     * @return  int
     */ 
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * Set the value of rooms
     *
     * @param  int  $rooms
     *
     * @return  self
     */ 
    public function setRooms(int $rooms)
    {
        $this->rooms = $rooms;

        return $this;
    }
}