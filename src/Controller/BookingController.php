<?php

namespace App\Controller;

use App\Entity\Ad;
use App\Entity\Booking;
use App\Form\BookingType;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Comment;
use App\Form\CommentType;

class BookingController extends AbstractController
{
    /**
     * @Route("/ads/{slug}/book", name="booking_create")
     * @Security("is_granted('ROLE_USER') and user !== ads.getAuthor()")
     */
    public function book(Ad $ads, Request $request, ObjectManager $manager)
    {

        $booking = new Booking();
        $form   = $this->createForm(BookingType::class, $booking);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $user = $this->getUser();
            $booking->setBooker($user)
                    ->setAd($ads)
            ;

            if (!$booking->isBookAbleDays())
            {
                $this->addFlash('warning',
                 'les dates que vous avez choisi ne peuvent etre reserves : elles sont deja prises'   
                );
            } else {
                $manager->persist($booking);
                $manager->flush();
                $this->addFlash('success',
                    'reservation a ete avec succes'
                );
                return $this->redirectToRoute("booking_show", ["id" => $booking->getId()]);            
            }

        }
        
        return $this->render('booking/book.html.twig', [
            'ads' => $ads,
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/booking/{id}", name="booking_show")
     * @Security("is_granted('ROLE_USER') and user == booking.getBooker()")
     */
    public function show(Booking $booking, Request $request, ObjectManager $manager)
    {

        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {   
            $comment->setAd($booking->getAd())
                    ->setAuthor($this->getUser())
            ;

            $manager->persist($comment);
            $manager->flush();

            $this->addFlash('success', 'Votre Commentaire a ete bien succes');

        }

        return $this->render('booking/show.html.twig', [
            'booking' => $booking,
            'form' => $form->createView()
        ]);        
    }
}
