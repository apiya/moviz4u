<?php

namespace App\Controller;

use App\Entity\Ad;
use App\Form\AdType;
use App\Entity\Image;
use App\Repository\AdRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Knp\Component\Pager\PaginatorInterface;
use App\Entity\AdSearch;
use App\Form\AdSearchType;

class AdController extends AbstractController
{
    /**
     * @Route("/ads", name="ads_home")
     */
    public function index(AdRepository $repo, Request $request, PaginatorInterface $paginator)
    {
        $adSearch   = new AdSearch(); 
        $form       = $this->createForm(AdSearchType::class, $adSearch);
        $form->handleRequest($request);

        $query   = $repo->findAllAd($adSearch);

        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            6/*limit per page*/
        );
        return $this->render('ad/index.html.twig', [
            'pagination' => $pagination,
            'search'     => $form->createView() 
        ]);
    }

    /**
     * @Route("/ads/new", name="ads_create")
     * @IsGranted("ROLE_USER")
     */
    public function create(Request $request, ObjectManager $em, UserInterface $user)
    {
        $ad = new Ad();
        $image = new Image();
        $form = $this->createForm(AdType::class, $ad);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid())
        {
            foreach ($ad->getImages() as $image) {
                $image->setAd($ad);
                $em->persist($image);
            }
            $ad->setAuthor($this->getUser());
            $em->persist($ad);
            $em->flush();

            $this->addFlash(
                "success",
                "Lannonce {$ad->getTitle()} a ete bien créer"
            );

            return $this->redirectToRoute('ads_show', [
                "slug" => $ad->getSlug()
            ]);

        }

        return $this->render('ad/new.html.twig', [
            'form' => $form->createView()
        ]);        
    }

    /**
     * @Route("/ads/{slug}/edit", name="ads_edit")
     * @Security("is_granted('ROLE_USER') and user == ad.getAuthor()")
     */
    public function edit(Ad $ad, Request $request, ObjectManager $em)
    {
        $form = $this->createForm(AdType::class, $ad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($ad->getImages() as $image) {
                $image->setAd($ad);
                $em->persist($image);
            }
            
            $em->persist($ad);
            $em->flush();

            $this->addFlash(
                "success",
                "Lannonce a ete bien editer"
            );

            return $this->redirectToRoute('ads_show', [
                "slug" => $ad->getSlug()
            ]);

        }       

        return $this->render('ad/edit.html.twig', [
            'form' => $form->createView(),
            'ad' => $ad
        ]);             
    }


    /**
     * @Route("/ads/{slug}", name="ads_show")
     */
    public function show(Ad $ads)
    {
        return $this->render('ad/show.html.twig', [
            'ads' => $ads
        ]);        
    }

    /**
     * @Route("/ads/{id}/delete", name="ads_delete")
     * @Security("IsGranted('ROLE_USER') AND user == ads.getAuthor()")
     */
    public function delete(Ad $ads, ObjectManager $manager)
    {

        $manager->remove($ads);
        $manager->flush();
        $this->addFlash("success", "cette annonce ete suprimmer avec succes");
        return $this->redirectToRoute("homepage");

    }
}
