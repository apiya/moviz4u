<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\AdRepository;
use App\Entity\Ad;
use App\Form\AdType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

class AdminAdController extends AbstractController
{
    /**
     * @Route("/admin/ads", name="admin_ads")
     */
    public function index(AdRepository $repo)
    {
        $ads = $repo->findAll();
        return $this->render('admin/ads/index.html.twig', [
            "ads" => $ads
        ]);
    }

    /**
     * @Route("/admin/ads/edit/{slug}", name="admin_ads_edit")
     */
    public function edit(Ad $ad, Request $request, ObjectManager $em)
    {
        $form = $this->createForm(AdType::class, $ad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($ad->getImages() as $image) {
                $image->setAd($ad);
                $em->persist($image);
            }

            $em->persist($ad);
            $em->flush();

            $this->addFlash(
                "success",
                "Lannonce a ete bien editer"
            );

            return $this->redirectToRoute('admin_ads');
        }
        
        return $this->render('admin/ads/edit.html.twig', [
            "form" => $form->createView(),
            "ads" => $ad
        ]);
    }

    /**
     * @Route("/admin/ads/delete/{id}", name="admin_ads_delete")
     */
    public function delete(Ad $ad, ObjectManager $em)
    {
        $em->remove($ad);
        $em->flush();
        $response = new Response();
        return $response->send();

        $this->addFlash('success', "l'annonce a ete bien supprimer");
    }

}
