<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;
use App\Services\StatsService;

class AdminDashboardController extends AbstractController
{
    /**
     * @Route("/admin/dashboard", name="admin_dashboard")
     */
    public function index(ObjectManager $em, StatsService $statsService)
    {
        $users      = $statsService->getCountUser();
        $post       = $statsService->getCountAds();
        $booking    = $statsService->getCountBooking();
        $comment    = $statsService->getCountBooking();
        $bestAds    = $statsService->getAds('DESC');
        $notAds     = $statsService->getAds('ASC');
        
        return $this->render('admin/dashboard.html.twig', [
            "users"     => $users,
            "post"      => $post,
            "comment"   => $comment,
            "booking"   => $booking,
            "bestAds"   => $bestAds,
            "notAds"    => $notAds
        ]);
    }
}
