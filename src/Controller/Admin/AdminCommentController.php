<?php

namespace App\Controller\Admin;

use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminCommentController extends AbstractController
{
    /**
     * @Route("/admin/comments", name="admin_comment")
     */
    public function index(CommentRepository $repo)
    {
        $comment = $repo->findAll();
        return $this->render('admin/comment/index.html.twig', [
            'comments' => $comment,
        ]);
    }

    /**
     * @Route("/admin/comments/edit/{id}", name="admin_comment_edit")
     */
    public function edit(Comment $comment, Request $request, ObjectManager $em)
    {   
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em->persist($comment);
            $em->flush();
            return $this->redirectToRoute("admin_comment");
        }

        return $this->render("admin/comment/edit.html.twig", [
            'form' => $form->createView(),
            "comment" => $comment
        ]);

    }

    /**
     * @Route("/admin/comment/delete/{id}", name="admin_comment_delete")
     */
    public function delete(Comment $comment, ObjectManager $em)
    {
        $em->remove($comment);
        $em->flush();
        $response = new Response();
        return $response->send();
    }
}
