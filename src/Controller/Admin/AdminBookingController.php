<?php

namespace App\Controller\Admin;

use App\Entity\Booking;
use App\Form\AdminBookingType;
use App\Repository\BookingRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;

class AdminBookingController extends AbstractController
{
    /**
     * @Route("/admin/booking", name="admin_booking")
     */
    public function index(BookingRepository $repo)
    {
        $booking = $repo->findAll();
        return $this->render('admin/booking/index.html.twig', [
            "bookings" => $booking
        ]);
    }

    /**
     * @Route("/admin/booking/edit/{id}", name="admin_booking_edit")
     */
    public function edit(Booking $booking, Request $request, ObjectManager $em)
    {
        $form = $this->createForm(AdminBookingType::class, $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em->persist($booking);
            $em->flush();

            return $this->redirectToRoute("admin_booking");
        }

        return $this->render('admin/booking/edit.html.twig', [
            "form" => $form->createView(),
            "booking" => $booking
        ]);

    }

    /**
     * @Route("/admin/booking/delete/{id}", name="admin_booking_delete")
     */
    public function delete(Booking $booking, ObjectManager $em)
    {
        $em->remove($booking);
        $em->flush();
        $response = new Response();
        return $response->send();
    }

}
