<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\AccountType;
use App\Entity\UpdatePassword;
use App\Form\RegistrationType;
use App\Form\UpdatePasswordType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AccountController extends AbstractController
{
    /**
     * @Route("/login", name="account_login")
     */
    public function login(AuthenticationUtils $utils)
    {
        $error = $utils->getLastAuthenticationError();
        $username = $utils->getLastUsername();
        return $this->render('account/login.html.twig',[
            'hasErr' => $error !== null,
            'username' => $username
        ]);
    }

    /**
     * @Route("/logout", name="account_logout")
     */
    public function logout()
    {   

    }

    /**
     * @Route("/register", name="account_register")
     */
    public function register(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {

        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            $user->setIsOnline(0);
            $manager->persist($user);
            $manager->flush();

            $this->addFlash('success', 'votre compte a bien ete creer ! vous pouvez maintenant vous connecter');
            return $this->redirectToRoute('account_login');
        }

        return $this->render('account/register.html.twig', [
            'form' => $form->createView()
        ]);   

    }

    /**
     * @Route("/account/profile", name="account_profile")
     * @IsGranted("ROLE_USER")
     */
    public function profile(Request $request, ObjectManager $manager)
    {
        $user = $this->getUser();

        $form = $this->createForm(AccountType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $manager->persist($user);
            $manager->flush();

            $this->addFlash('success', 'les donnees de profile ont ete enregistree avec succes');

        }

        return $this->render('account/profile.html.twig', [
            'form' => $form->createView()
        ]);           
    }

    /**
     * @Route("/account/password", name="account_password")
     * @IsGranted("ROLE_USER")
     */
    public function updatePassword(Request $request, UserPasswordEncoderInterface $encoder, ObjectManager $manager)
    {

        $user = $this->getUser();
        $password = new UpdatePassword();
        $form = $this->createForm(UpdatePasswordType::class, $password);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {

            if (!password_verify($password->getOldPassword(), $user->getPassword()))
            {
                // Error
                $form->get('oldPassword')->addError(new FormError("ancien mot de passe n'es pas corecct"));

            } else {
            
                $newPass = $password->getNewPassword();
                $hash = $encoder->encodePassword($user, $newPass);
                $user->setPassword($hash);
                $manager->persist($user);
                $manager->flush();

                $this->addFlash("success", "mot de passe de profile ont ete enregistree avec succes");
                
            }
            
        }

        return $this->render('account/password.html.twig', [
            'form' => $form->createView()
        ]);         
    }

    /**
     * @Route("/account", name="account_show")
     * @IsGranted("ROLE_USER")
     */
    public function myAccount()
    {
        return $this->render('user/show.html.twig', [
            "user" => $this->getUser()
        ]);
    }

    /**
     * @Route("/account/booking", name="account_booking")
     */
    public function myBook()
    {
        return $this->render('booking/index.html.twig');
    }

}
