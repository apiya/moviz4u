<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\UpdatePassword;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UpdatePasswordType extends AbstractType
{

    private function getConfig($label, $placeholder)
    {   
        return [
            'label' => $label,
            'attr'  => ['placeholder' => $placeholder]
        ];
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('oldPassword', PasswordType::class, $this->getConfig("Ancien mot de passe", 'Ancien mot de passe'))
            ->add('newPassword', PasswordType::class, $this->getConfig("Nouveau mot de passe", 'Nouveau mot de passe'))
            ->add('confirmPassword', PasswordType::class, $this->getConfig("Confirm mot de passe", 'Confirm mot de passe'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UpdatePassword::class
        ]);
    }
}
