<?php

namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;


class FrDateTransformer implements DataTransformerInterface
{

    public function transform($date)
    {

        if ($date === null) {
            return '';
        }

        return $date->format('d/m/Y');

    }

    public function reverseTransform($frDate)
    {

        if (!$frDate)
        {
            return;
        }

        $date = \DateTime::createFromFormat('d/m/Y', $frDate);

        if ($date === false) {
            throw new TransformationFailedException('cette date est incorrect');
        }

        return $date;

    }

}