<?php

namespace App\Form;

use App\Entity\Ad;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class AdType extends AbstractType
{
    private function getConfig($label, $placeholder)
    {
        return [
            'label' => $label,
            'attr' => [
                'placeholder' => $placeholder
            ]
        ];
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, $this->getConfig('Titre', 'Titre'))
            ->add('slug', TextType::class, $this->getConfig('Slug', 'Slug'))
            ->add('description', TextareaType::class, $this->getConfig('Description', 'Description'))
            ->add('introduction', TextType::class, $this->getConfig('Introduction', 'Introduction'))
            ->add('coverImage', UrlType::class, $this->getConfig('Cover Image', 'Cover Image'))
            ->add('rooms', IntegerType::class)
            ->add('price', MoneyType::class, $this->getConfig('Prix', 'Entre Un Prix'))
            ->add('images', CollectionType::class, [
                "entry_type" => ImageType::class,
                'label' => "Images",
                'allow_add' => true,
                'allow_delete' => true,
                'required' => false,
            ])    
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ad::class,
        ]);
    }
}
