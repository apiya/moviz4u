<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, $this->getConfig('Nom', 'Nom'))
            ->add('lastName', TextType::class, $this->getConfig('Prenom', 'Prenom'))
            ->add('email', EmailType::class, $this->getConfig('Adresse email', 'Adresse email'))
            ->add('picture', UrlType::class, $this->getConfig('Url avatar', 'Photo de profil'))
            ->add('password', PasswordType::class, $this->getConfig('Mot de passe', 'Mot de passe'))
            ->add('confirmPassword', PasswordType::class, $this->getConfig(' Confirm Mot de passe', 'Confirm Mot de passe'))
            ->add('introduction', TextareaType::class, $this->getConfig('Introduction', 'Itroduction'))
            ->add('description', TextareaType::class, $this->getConfig('Description', 'Description'))
            ->add('slug')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

    private function getConfig($placeholder, $label)
    {
        return [
            'label' => $label,
            'attr' => ['placeholder' => $placeholder]
        ];
    }

}
