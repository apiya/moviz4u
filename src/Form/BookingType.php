<?php

namespace App\Form;

use App\Entity\Booking;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Form\DataTransformer\FrDateTransformer;

class BookingType extends AbstractType
{

    private $transformer;

    public function __construct(FrDateTransformer $transformer)
    {
       $this->transformer = $transformer;     
    }

    private function getConfig($label, $placeholder, $options=[])
    {
        return array_merge([
            'label' => $label,
            'attr'  => ['placeholder' => $placeholder],
        ],$options);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate', TextType::class, $this->getConfig("Date d'arrivee", "Date d'arrivee"))
            ->add('endDate', TextType::class,  $this->getConfig("Date de depart", "Date de depart"))
            ->add('comments', TextareaType::class,  $this->getConfig("Commentaire", "Commentaire", ["required"=>false]))
        ;
        $builder->get('startDate')->addModelTransformer($this->transformer);
        $builder->get('endDate')->addModelTransformer($this->transformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Booking::class,
            'validation_groups' => ["Default", "front"]
        ]);
    }
}
