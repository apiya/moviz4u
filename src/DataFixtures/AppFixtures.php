<?php

namespace App\DataFixtures;

use App\Entity\Ad;
use Faker\Factory;
use App\Entity\User;
use App\Entity\Image;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\Role;
use App\Entity\Booking;
use App\Entity\Comment;

class AppFixtures extends Fixture
{

    private $encoderPassword;

    public function __construct(UserPasswordEncoderInterface $encoderPassword)
    {
        $this->encoderPassword = $encoderPassword;
    }

    public function load(ObjectManager $manager)
    {

        $faker = Factory::create('FR-fr');

        $adminRole = new Role();
        $adminRole->setTitle("ROLE_ADMIN");
        $manager->persist($adminRole);

        $userAdmin = new User();
        $userAdmin->setFirstName("Said")
                ->setLastName("Mounaim")
                ->setEmail("saidmounaim00@gmail.com")
                ->setPassword($this->encoderPassword->encodePassword($userAdmin, 'apiyaapiya'))
                ->setIntroduction($faker->text($maxNbChars = 100))
                ->setDescription($faker->text($maxNbChars = 300))
                ->setPicture("https://placehold.it/50x50")
                ->addUserRole($adminRole);
        $manager->persist($userAdmin);    

        $users = [];
        $genres = ['male', 'female'];
        $slugify = new Slugify();

        for ($i = 0; $i < 10; $i++) 
        {
            $user = new User();
            $genre = $faker->randomElement($genres);
            $picture = 'https://randomuser.me/api/portraits/';
            $pictureID = $faker->numberBetween('1', '99') . '.jpg';
            if ($genre === 'male') {
                $picture .= "men/$pictureID";
            } elseif ($genre === 'female') {
                $picture .= "women/$pictureID";
            }

            $user->setFirstName($faker->firstName($genre))
                ->setLastName($faker->lastName($genre))
                ->setEmail($faker->email)
                ->setPassword($this->encoderPassword->encodePassword($user, 'apiyaapiya'))
                ->setIntroduction($faker->text($maxNbChars = 100))
                ->setDescription($faker->text($maxNbChars = 300))
                ->setPicture($picture);
            $manager->persist($user);
            $users[] = $user;
        }

        for ($i = 0; $i < 30; $i++) 
        {
            $ad = new Ad();
            $user = $users[mt_rand(1, 9)];
            $ad->setTitle($faker->sentence())
                ->setSlug($slugify->slugify($faker->sentence()))
                ->setPrice($faker->numberBetween($min = 10, $max = 100))
                ->setIntroduction($faker->text($maxNbChars = 100))
                ->setDescription($faker->text($maxNbChars = 300))
                ->setCoverImage($faker->imageUrl($width = 640, $height = 480))
                ->setRooms($faker->numberBetween($min = 1, $max = 9))
                ->setAuthor($user);

                for ($j=0; $j < mt_rand(1,3) ; $j++) 
                { 
                    $image = new Image();
                    $image->setImage(mt_rand(1, 10).".jpeg")
                          ->setCaption("Title Image $j")
                          ->setAd($ad)
                    ;
                    $manager->persist($image);
                }

                for ($j=0; $j <= mt_rand(0, 9) ; $j++) 
                { 
                    $booking    = new Booking();
                    $createdAt  = $faker->dateTimeBetween('-6 months'); 
                    $startDate  = $faker->dateTimeBetween('-3 months'); 
                    $duration   = mt_rand(3, 10);
                    $endDate    = (clone $startDate)->modify("+$duration days");
                    $amount     = $ad->getPrice() * $duration;
                    $booker     = $users[mt_rand(0, count($users) - 1 )];

                    $booking->setBooker($booker)
                            ->setAd($ad)
                            ->setCreatedAt($createdAt)
                            ->setStartDate($startDate)
                            ->setEndDate($endDate)
                            ->setAmount($amount)
                            ->setComments($faker->text($maxNbChars = 300))
                    ;

                    $manager->persist($booking);

                }


                for ($j=0; $j < mt_rand(1, 10) ; $j++) { 
                   $comment = new Comment();
                   $author = $users[mt_rand(0, count($users) - 1)];
                   $comment->setCreatedAt(new \DateTime('now'))
                           ->setContent($faker->paragraph())
                           ->setAuthor($author)
                           ->setRating(mt_rand(1, 5))
                           ->setAd($ad)
                   ;
                   $manager->persist($comment);        
                }


            $manager->persist($ad);

        }

        $manager->flush();
    }

}
