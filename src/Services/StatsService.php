<?php

namespace App\Services;

use Doctrine\Common\Persistence\ObjectManager;


class StatsService 
{

    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }


    public function getCountUser()
    {
        return $this->manager->createQuery('SELECT count(u) FROM App\Entity\User u')->getSingleScalarResult();
    }

    public function getCountComment()
    {
        return $this->manager->createQuery('SELECT count(b) FROM App\Entity\Booking b')->getSingleScalarResult();
    }

    public function getCountAds()
    {
        return $this->manager->createQuery('SELECT count(a) FROM App\Entity\Ad a')->getSingleScalarResult();
    }

    public function getCountBooking()
    {
        return $this->manager->createQuery('SELECT count(b) FROM App\Entity\Booking b')->getSingleScalarResult();
    }

    public function getAds($order)
    {
        return $this->manager->createQuery("
            SELECT avg(c.rating) as note, u.firstName, u.lastName, a.id, a.title FROM App\Entity\Comment c
            JOIN c.ad a
            JOIN a.author u
            GROUP BY a
            ORDER BY note $order")->setMaxResults(5)->getResult();
    }

    
}