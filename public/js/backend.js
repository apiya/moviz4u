$(document).ready(function () {
    $('.table-admin').DataTable({
        "pageLength": 5,
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bAutoWidth": false,
        "bDestroy": true
    });
});

$(document).on('click', '.AdminDltBook', function (e) {
    e.preventDefault();

    const id = $(this).data('id');
    const $tr = $(this).closest('tr');

    if (confirm('vous etes sur supprimer cette reservation??')) {
        fetch(`/admin/booking/delete/${id}`, {
            method: "DELETE",
        }).then(function () {
            // $('#ads-' + id).fadeOut("slow");
            $tr.find('td').fadeOut("slow");
        })
    }
})

$(document).on('click', '.AdminDltAds', function (e) {
    e.preventDefault();

    const id = $(this).data('id');
    const $tr = $(this).closest('tr');

    if (confirm('vous etes sur supprimer cette annonce?')) {
        fetch(`/admin/ads/delete/${id}`, {
            method: "DELETE",
        }).then(function () {
            // $('#ads-' + id).fadeOut("slow");
            $tr.find('td').fadeOut("slow");
        })
    }
})

$(document).on('click', '.AdminDltComm', function (e) {
    e.preventDefault();

    const id = $(this).data('id');
    const $tr = $(this).closest('tr');

    if (confirm('vous etes sur supprimer cette commentaire?')) {
        fetch(`/admin/comment/delete/${id}`, { method: "DELETE" }).then(function () {
            // $('#ads-' + id).fadeOut("slow");
            $tr.find('td').fadeOut("slow");
        })
    }
})
