
        $('#btn-addimage').click(function () {

            const index = +$('#counter-input').val();
            const data = $('#ad_images').data('prototype').replace(/__name__/g, index);


            $('#ad_images').append(data);
            $('#counter-input').val(index + 1);

            handelBtnDelete()

        })

        function handelBtnDelete()
        {
            $('span[data-action="delete"]').click(function () {
                const form = this.dataset.target;
                $(form).remove();
            })            
        }

        function updateHandel()
        {
            const count = $('#ad_images div.form-group').length;
            $('#counter-input').val(count);
        }

        updateHandel()
        handelBtnDelete()
